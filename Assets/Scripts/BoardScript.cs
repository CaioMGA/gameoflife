﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class BoardScript : MonoBehaviour
{
    public Slider SpeedSlider; 
    public List<AudioClip> sounds;
    public List<AudioClip> lastHits;
    private List<List<PlayMakerFSM>> cells;
    private int LINES = 0;
    private int ROWS = 0;
    private int soundClipCount;
    private bool playing = false;
    private float simulationSpeed = 0.3f;
    private float simulationElapsedTime = 0f;
    private AudioSource audioSource;
    private int soundIndex = -1;
    private int lastHitIndex = 0;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        soundClipCount = sounds.Count;
        cells = new List<List<PlayMakerFSM>>();
        int lineCount = transform.childCount;

        for (var i = 0; i < lineCount; i++)
        {
            var childrenOfTheLine = transform.GetChild(i).GetComponentsInChildren<PlayMakerFSM>();
            var line = childrenOfTheLine.ToList();
            cells.Add(line);
        }
        
        SpeedSlider.onValueChanged.AddListener(UpdateSpeed);

    }

    private void Tick()
    {
        var newCellsState = new bool[LINES, ROWS]; 
        
        // calculates next gen
        for (var i = 0; i < LINES; i++)
        {
            for (var j = 0; j < ROWS; j++)
            {
                newCellsState[i, j] = GetCellStateOnNextGen(i, j);
            }
        }
        
        //applies next gen
        for (var i = 0; i < LINES; i++)
        {
            for (var j = 0; j < ROWS; j++)
            {
                var fsmEvent = newCellsState[i, j] ? "Turn On" : "Turn Off";
                cells[i][j].SendEvent(fsmEvent);
            }
        }

        
        soundIndex++;
        if (soundIndex > soundClipCount)
        {
            soundIndex = 0;
        }
        
        if (soundIndex == soundClipCount)
        {
            audioSource.PlayOneShot(lastHits[lastHitIndex]);
            lastHitIndex++;
            if (lastHitIndex >= 2)
            {
                lastHitIndex = 0;
            }
        }
        else
        {
            audioSource.PlayOneShot(sounds[soundIndex]);
        }
        
    }

    public void UpdateSpeed(float value)
    {
        simulationSpeed = value/1000f;
        Debug.Log(simulationSpeed);
    }
    private void FixedUpdate()
    {
        if (playing)
        {
            simulationElapsedTime += Time.fixedDeltaTime;
            if (simulationElapsedTime >= simulationSpeed)
            {
                simulationElapsedTime = 0;
                Tick();
            }
        }
    }

    public void PlayBoard()
    {
        if (playing)
        {
            playing = false;
            simulationElapsedTime = 0;
            return;
        }

        playing = true;
        LINES = transform.childCount;
        ROWS = cells[0].Count;
    }

    private bool GetCellStateOnNextGen(int x, int y)
    {
        var neighbors = 0;
        var isCellCurrentAlive = cells[x][y].FsmVariables.GetFsmBool("State").Value;
        if (x > 0)
        {
            if (cells[x - 1][y].FsmVariables.GetFsmBool("State").Value)
            {
                neighbors++;
            }

            if (y > 0)
            {
                if (cells[x - 1][y - 1].FsmVariables.GetFsmBool("State").Value)
                {
                    neighbors++;
                }
            }

            if (y < ROWS - 1)
            {
                if (cells[x - 1][y + 1].FsmVariables.GetFsmBool("State").Value)
                {
                    neighbors++;
                }
            }
        }
        
        if (x < LINES - 1)
        {
            if (cells[x + 1][y].FsmVariables.GetFsmBool("State").Value)
            {
                neighbors++;
            }

            if (y > 0)
            {
                if (cells[x + 1][y - 1].FsmVariables.GetFsmBool("State").Value)
                {
                    neighbors++;
                }
            }

            if (y < ROWS - 1)
            {
                if (cells[x + 1][y + 1].FsmVariables.GetFsmBool("State").Value)
                {
                    neighbors++;
                }
            }
        }

        if (y > 0)
        {
            if (cells[x][y - 1].FsmVariables.GetFsmBool("State").Value)
            {
                neighbors++;
            }
        }
        
        if (y < ROWS - 1)
        {
            if (cells[x][y + 1].FsmVariables.GetFsmBool("State").Value)
            {
                neighbors++;
            }
        }

        if (!isCellCurrentAlive)
        {
            return neighbors == 3;
        }
        
        return neighbors <= 3 && neighbors >= 2;
    }
}
